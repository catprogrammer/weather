#include <Arduino.h>
#include "windSpeed.h"
#include "windDirectionSensor.h"
#include "tippingBucketRainGauge.h"
#include "AS5600.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <clientApi.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"

#include <SoftwareSerial.h>
#include <MHZ.h>

#define CO2_IN 10

#define MH_Z19_RX D7 // D7
#define MH_Z19_TX D6 // D6

MHZ co2(MH_Z19_RX, MH_Z19_TX, CO2_IN, MHZ::MHZ19B);

void setup()
{
  Serial.begin(9600);
  Serial.println(" s");
  if (co2.isPreHeating())
  {
    Serial.print("Preheating");
    while (co2.isPreHeating())
    {
      Serial.print(".");
      delay(5000);
    }
    Serial.println();
  }

  delay(1000);
}

void loop()
{
  Serial.print("\n----- Time from start: ");
  // Serial.print(millis() / 1000);
  // Serial.println(" s");

  // int ppm_uart = co2.readCO2UART();
  // Serial.print("PPMuart: ");

  // if (ppm_uart > 0)
  // {
  //   Serial.print(ppm_uart);
  // }
  // else
  // {
  //   Serial.print("n/a");
  // }

  // int ppm_pwm = co2.readCO2PWM();
  // Serial.print(", PPMpwm: ");
  // Serial.print(ppm_pwm);

  // int temperature = co2.getLastTemperature();
  // Serial.print(", Temperature: ");

  // if (temperature > 0)
  // {
  //   Serial.println(temperature);
  // }
  // else
  // {
  //   Serial.println("n/a");
  // }

  // Serial.println("\n------------------------------");
  delay(5000);
}
