import tw from 'twin.macro'

export const Wrapper = tw.main`pt-[80px] pl-[300px] [background: #F5F4F6] min-h-screen pb-16`

export const Container = tw.div`mx-16 mt-16`
