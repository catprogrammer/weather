import tw from 'twin.macro'

export const Container = tw.header`flex items-center w-full h-[80px] pl-[365px] fixed z-40 [background: #FCFBFD] drop-shadow-xl`
