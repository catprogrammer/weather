import tw from 'twin.macro'

// export const Container = tw.header`flex items-center w-full h-[80px] pl-[400px] fixed bg-slate-50 drop-shadow-xl`

export const Container = tw.header`w-[300px] h-full fixed z-50 bg-slate-50`

export const Logo = tw.img`w-[70px] h-[70px] m-5`
