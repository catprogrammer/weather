import { Global } from '@emotion/react'
import React, { useEffect, useLayoutEffect } from 'react'
import tw, { css, theme, GlobalStyles as BaseStyles } from 'twin.macro'

const GlobalStyles = () => {
  return (
    <>
      <BaseStyles />
    </>
  )
}

export default GlobalStyles
