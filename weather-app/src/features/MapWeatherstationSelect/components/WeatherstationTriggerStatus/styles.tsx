import tw from 'twin.macro'

export const Container = tw.div`flex items-center h-[45px] px-5 bg-gray-200 rounded-2xl cursor-pointer`

export const Title = tw.div`text-base font-medium text-gray-800`
